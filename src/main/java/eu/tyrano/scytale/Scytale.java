package eu.tyrano.scytale;

import com.mojang.logging.LogUtils;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.slf4j.Logger;

@Mod(Scytale.MODID)
@Mod.EventBusSubscriber(value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD, modid = Scytale.MODID)
public class Scytale {

    public static final String MODID = "scytale";
    public static final Logger LOGGER = LogUtils.getLogger();

    public Scytale() {
        MinecraftForge.EVENT_BUS.register(new ClientEvents());
        IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();
        modEventBus.addListener(KeyMappings::registerBindings);
    }
}
