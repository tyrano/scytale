package eu.tyrano.scytale;

import com.google.common.primitives.Ints;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.ChatScreen;
import net.minecraft.client.gui.screens.InBedChatScreen;
import net.minecraft.network.chat.ClickEvent;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.util.StringUtil;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.client.event.ScreenEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Matcher.quoteReplacement;

public class ClientEvents {
    private static final Pattern SHARE_PATTERN = Pattern.compile("s@.*?@");
    private static final Pattern ACCEPT_PATTERN = Pattern.compile("a@.*?@");
    private static final Pattern ACCEPT_ID_PATTERN = Pattern.compile("a@-?\\d+?@");
    private static final Pattern CONFIRM_PATTERN = Pattern.compile("c@.*?@");
    private static final Pattern CONFIRM_ID_PATTERN = Pattern.compile("c@-?\\d+?@");
    private static final Pattern ENCRYPT_PATTERN = Pattern.compile("e@.*?@");
    private static final SecureRandom secureRandom;

    static {
        byte[] randomSeed = new byte[32];
        new Random().nextBytes(randomSeed);
        secureRandom = new SecureRandom(randomSeed);
    }

    private PrivateKey privateKey;
    private SecretKey symmetricKey;
    private int channelId;
    private int personalId;
    private final Map<Integer, PublicKey> publicKeys = new LinkedHashMap<>() {
        @Override
        protected boolean removeEldestEntry(Map.Entry<Integer, PublicKey> eldest) {
            return this.size() > 8;
        }
    };

    @SubscribeEvent
    public void onOpenChatScreen(ScreenEvent.Opening event) {
        if (event.getNewScreen() != null && event.getNewScreen() instanceof ChatScreen) {
            if (event.getNewScreen().getClass() == ChatScreen.class) {
                String initial = ((ChatScreen) event.getNewScreen()).initial;
                event.setNewScreen(new ChatScreen(initial) {
                    @Override
                    public boolean handleChatInput(@NotNull String message, boolean log) {
                        return processMessage(message, log, this.minecraft, this);
                    }
                });
            } else if (event.getNewScreen().getClass() == InBedChatScreen.class) {
                event.setNewScreen(new InBedChatScreen() {
                    @Override
                    public boolean handleChatInput(@NotNull String message, boolean log) {
                        return processMessage(message, log, this.minecraft, this);
                    }
                });
            } else {
                Scytale.LOGGER.warn("Unsupported chat screen: message won't be encrypted!");
            }
        }
    }

    private boolean processMessage(String message, boolean log, Minecraft minecraft, ChatScreen screen) {
        if (message.isEmpty()) {
            return true;
        }

        message = StringUtil.trimChatMessage(StringUtils.normalizeSpace(message.trim()));

        assert minecraft != null;
        assert minecraft.player != null;

        if (log) {
            minecraft.gui.getChat().addRecentChat(message);
        }

        message = onMessageSend(message);
        if (message.length() > 256) {
            return true;
        }

        if (message.startsWith("/")) {
            minecraft.player.connection.sendCommand(message.substring(1));
        } else {
            minecraft.player.connection.sendChat(message);
        }

        return minecraft.screen == screen;
    }

    public String onMessageSend(String original) {
        try {
            Matcher m = SHARE_PATTERN.matcher(original);
            Matcher m2 = ACCEPT_ID_PATTERN.matcher(original);
            Matcher m3 = CONFIRM_ID_PATTERN.matcher(original);
            Matcher m4 = ENCRYPT_PATTERN.matcher(original);
            if (m.find()) {
                byte[] keyBytes = new byte[32];
                secureRandom.nextBytes(keyBytes);
                this.symmetricKey = new SecretKeySpec(keyBytes, "AES");

                this.channelId = secureRandom.nextInt();
                return m.replaceFirst(quoteReplacement("s@" + bytesToChat(new byte[0], this.channelId) + "@"));
            } else if (m2.find()) {
                this.personalId = secureRandom.nextInt();
                this.channelId = Integer.parseInt(m2.group().replace("a@", "").replace("@", ""));
                KeyPair pair = generateKeyPair();
                this.privateKey = pair.getPrivate();
                return m2.replaceFirst(quoteReplacement("a@" + bytesToChat(pair.getPublic().getEncoded(),
                        this.channelId, this.personalId) + "@"));
            } else if (m3.find()) {
                int userId = Integer.parseInt(m3.group().replace("c@", "").replace("@", ""));
                Key key = publicKeys.get(userId);
                Cipher cipher = Cipher.getInstance(key.getAlgorithm());
                cipher.init(Cipher.ENCRYPT_MODE, key);
                byte[] cipherText = cipher.doFinal(this.symmetricKey.getEncoded());
                return m3.replaceFirst(quoteReplacement("c@" + bytesToChat(cipherText, this.channelId, userId) + "@"));
            } else if (this.symmetricKey != null) {
                Cipher cipher = Cipher.getInstance("AES");
                cipher.init(Cipher.ENCRYPT_MODE, this.symmetricKey);
                return m4.replaceAll(matchResult -> {
                    try {
                        return quoteReplacement("e@" + bytesToChat(cipher
                                .doFinal(matchResult.group().replaceFirst("e@", "")
                                        .replace("@", "").getBytes(StandardCharsets.UTF_8)), this.channelId) + "@");
                    } catch (GeneralSecurityException e) {
                        Scytale.LOGGER.debug("An error occurred while encrypting a message", e);
                        return "####";
                    }
                });
            }
        } catch (GeneralSecurityException | NumberFormatException | IndexOutOfBoundsException | NegativeArraySizeException e) {
            Scytale.LOGGER.debug("An error occurred while encrypting a message", e);
            return null;
        }
        return original;
    }

    public static KeyPair generateKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(1024);
        return keyPairGenerator.generateKeyPair();
    }

    private static String bytesToChat(byte[] in, int... ids) {

        byte[] tmp = new byte[in.length + ids.length * 4 + 2];
        tmp[0] = (byte) ids.length;
        for (int i = 0; i < ids.length; i++) {
            System.arraycopy(Ints.toByteArray(ids[i]), 0, tmp, i * 4 + 1, 4);
        }
        System.arraycopy(in, 0, tmp, ids.length * 4 + 1, in.length);
        in = tmp;

        int length = in.length;
        int mod = length % 3;

        if (mod != 0) {
            byte[] copy = new byte[(Math.floorDiv(length, 3) + 1) * 3];
            System.arraycopy(in, 0, copy, 0, length);
            Arrays.fill(copy, length, copy.length - 1, (byte) 0);
            in = copy;
            length += 3 - mod;
        }

        in[length - 1] = (byte) ((3 - mod) % 3);

        char[] out = new char[length / 3 * 4];
        int index = 0;
        for (int i = 0; i < length; i += 3) {
            int part1 = (in[i] & 0xFF);
            int part2 = ((in[i + 1] & 0xFF) << 8);
            int part3 = ((in[i + 2] & 0xFF) << 16);

            int num = part1 | part2 | part3;

            out[index++] = (char) (33 + (byte) ((num & 0xFC0000) >> 18));
            out[index++] = (char) (33 + (byte) ((num & 0x3F000) >> 12));
            out[index++] = (char) (33 + (byte) ((num & 0xFC0) >> 6));
            out[index++] = (char) (33 + (byte) ((num & 0x3F)));
        }
        return new String(out).replace('@', '°');
    }

    private static Pair<int[], byte[]> chatToBytes(String str) {
        char[] in = str.replace('°', '@').toCharArray();
        int length = in.length;

        byte[] out = new byte[length / 4 * 3];
        int index = 0;
        for (int i = 0; i < length; i += 4) {
            int part1 = (((in[i] - 33))) << 18;
            int part2 = (((in[i + 1] - 33))) << 12;
            int part3 = (((in[i + 2] - 33))) << 6;
            int part4 = (((in[i + 3] - 33)));

            int num = part1 | part2 | part3 | part4;
            out[index++] = (byte) (num);
            out[index++] = (byte) (num >> 8);
            out[index++] = (byte) (num >> 16);
        }

        byte overflow = out[out.length - 1];

        byte idCount = out[0];
        byte[] tmp = new byte[out.length - 4 * idCount - 2 - overflow];
        int[] ids = new int[idCount];
        for (int i = 0; i < idCount; i++) {
            ids[i] = Ints.fromBytes(out[i * 4 + 1], out[i * 4 + 2], out[i * 4 + 3], out[i * 4 + 4]);

        }
        System.arraycopy(out, 4 * idCount + 1, tmp, 0, tmp.length);
        out = tmp;

        return Pair.of(ids, out);
    }

    @SubscribeEvent
    public void onMessageReceived(ClientChatReceivedEvent event) {
        try {
            String original = event.getMessage().getString();
            Matcher m = SHARE_PATTERN.matcher(original);
            Matcher m2 = ACCEPT_PATTERN.matcher(original);
            Matcher m3 = CONFIRM_PATTERN.matcher(original);
            Matcher m4 = ENCRYPT_PATTERN.matcher(original);
            if (m.find()) {
                Pair<int[], byte[]> result = chatToBytes(m.group().replaceFirst("s@", "").replace("@", ""));

                if (result.getKey()[0] != this.channelId) {
                    Component clickable = Component.literal("[Accept]").withStyle(Style.EMPTY
                            .withBold(true)
                            .withColor(0xFF8838)
                            .withClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND,
                                    "a@" + result.getKey()[0] + "@"))
                    );
                    String text = m.replaceFirst("");
                    event.setMessage(Component.literal(text).append(clickable));
                } else {
                    event.setMessage(Component.literal("Channel invite sent"));
                }
            } else if (m2.find()) {
                Pair<int[], byte[]> result = chatToBytes(m2.group().replaceFirst("a@", "").replace("@", ""));
                if (result.getKey()[1] != this.personalId && result.getKey()[0] == this.channelId) {
                    int userID = result.getKey()[1];
                    KeyFactory kf = KeyFactory.getInstance("RSA");
                    this.publicKeys.put(userID, kf.generatePublic(new X509EncodedKeySpec(result.getValue())));

                    Component clickable = Component.literal("[Confirm]").withStyle(Style.EMPTY
                            .withBold(true)
                            .withColor(0xFF8838)
                            .withClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND,
                                    "c@" + userID + "@"))
                    );
                    String text = m2.replaceFirst("");
                    event.setMessage(Component.literal(text).append(clickable));
                } else {
                    event.setMessage(Component.literal("Channel invite accepted"));
                }
            } else if (m3.find()) {
                Pair<int[], byte[]> result = chatToBytes(m3.group().replaceFirst("c@", "").replace("@", ""));
                if (result.getKey()[1] == this.personalId && result.getKey()[0] == this.channelId) {
                    Cipher cipher = Cipher.getInstance(this.privateKey.getAlgorithm());
                    cipher.init(Cipher.DECRYPT_MODE, this.privateKey);
                    this.symmetricKey = new SecretKeySpec(cipher.doFinal(result.getValue()), "AES");

                    Component confirmation = Component.literal("Channel opened and working").withStyle(Style.EMPTY
                            .withBold(true)
                            .withColor(0xFF8838)
                            .withClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND,
                                    "e@Your message here@"))
                    );
                    event.setMessage(confirmation);
                } else if (result.getKey()[0] == this.channelId) {
                    event.setMessage(Component.literal("Channel invite confirmed"));
                } else {
                    event.setMessage(Component.literal("####"));
                }
            } else if (m4.find()) {
                Cipher cipher;
                if (this.symmetricKey != null) {
                    cipher = Cipher.getInstance("AES");
                    cipher.init(Cipher.DECRYPT_MODE, this.symmetricKey);
                } else {
                    cipher = null;
                }
                event.setMessage(Component.literal(m4.replaceAll(matchResult -> {
                    try {
                        if (cipher != null) {
                            Pair<int[], byte[]> result = chatToBytes(matchResult.group().replaceFirst("e@", "").replace("@", ""));
                            if (result.getKey()[0] == this.channelId) {
                                return quoteReplacement(new String(cipher.doFinal(result.getValue()), StandardCharsets.UTF_8));
                            }
                        }
                        return "####";
                    } catch (GeneralSecurityException e) {
                        Scytale.LOGGER.debug("An error occurred while decrypting a message", e);
                        return "####";
                    }
                })));
            }
        } catch (GeneralSecurityException | IndexOutOfBoundsException | NegativeArraySizeException e) {
            Scytale.LOGGER.debug("An error occurred while decrypting a message", e);
        }
    }

    @SubscribeEvent
    public void onClientTick(TickEvent.ClientTickEvent event) {
        if (event.phase == TickEvent.Phase.END) {
            while (KeyMappings.SHARE_MAPPING.get().consumeClick()) {
                Minecraft.getInstance().setScreen(new ChatScreen("s@_@"));
            }
        }
    }
}
