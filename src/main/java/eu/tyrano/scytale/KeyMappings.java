package eu.tyrano.scytale;

import com.mojang.blaze3d.platform.InputConstants;
import net.minecraft.client.KeyMapping;
import net.minecraftforge.client.event.RegisterKeyMappingsEvent;
import net.minecraftforge.common.util.Lazy;
import org.lwjgl.glfw.GLFW;

public class KeyMappings {
    public static final Lazy<KeyMapping> SHARE_MAPPING = Lazy.of(() -> new KeyMapping(
            "key.scytale.share",
            InputConstants.Type.KEYSYM,
            GLFW.GLFW_KEY_G,
            "key.categories.misc"
    ));

    public static void registerBindings(RegisterKeyMappingsEvent event) {
        event.register(SHARE_MAPPING.get());
    }
}
